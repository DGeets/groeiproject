package be.kdg.baseballproject.patterns;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.BaseballersInterface;

import java.util.List;
import java.util.Observable;

public class ObservableBaseballers extends Observable implements BaseballersInterface {
    private Baseballers baseballers;

    public ObservableBaseballers() {
        baseballers = new Baseballers();
    }

    @Override
    public boolean voegToe(Baseballer baseballer) {
        boolean gelukt = baseballers.voegToe(baseballer);
        if (gelukt){
            setChanged();
            notifyObservers("Toegevoegd: " + baseballer);
        }
        return gelukt;
    }

    @Override
    public boolean verwijder(String team, int teamNumber) {
        boolean gelukt = baseballers.verwijder(team, teamNumber);
        if (gelukt){
            setChanged();
            notifyObservers("Verwijderd: " + team + " " + teamNumber);
        }
        return gelukt;
    }

    @Override
    public Baseballer zoek(String team, int teamNumber) {
        return baseballers.zoek(team, teamNumber);
    }

    @Override
    public List<Baseballer> gesorteerdOpNaam() {
        return baseballers.gesorteerdOpNaam();
    }

    @Override
    public List<Baseballer> gesorteerdOpHomeruns() {
        return baseballers.gesorteerdOpHomeruns();
    }

    @Override
    public List<Baseballer> gesorteerdopGeboorte() {
        return baseballers.gesorteerdopGeboorte();
    }

    @Override
    public int getAantal() {
        return baseballers.getAantal();
    }
}
