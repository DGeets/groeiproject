package be.kdg.baseballproject.patterns;

import java.util.Observable;
import java.util.Observer;

public class BaseballersObserver implements Observer {
    @Override
    public void update(Observable o, Object arg) {
        System.out.println("OBSERVER: " + arg);
    }
}
