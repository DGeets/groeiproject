package be.kdg.baseballproject.model;

import java.util.List;

public interface BaseballersInterface {

    boolean voegToe(Baseballer baseballer);

    boolean verwijder(String team, int teamNumber);

    Baseballer zoek(String team, int teamNumber);

    List<Baseballer> gesorteerdOpNaam();

    List<Baseballer> gesorteerdOpHomeruns();

    List<Baseballer> gesorteerdopGeboorte();

    int getAantal();
}
