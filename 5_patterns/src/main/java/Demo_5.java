import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.BaseballerFactory;
import be.kdg.baseballproject.model.Sex;
import be.kdg.baseballproject.patterns.BaseballersObserver;
import be.kdg.baseballproject.patterns.ObservableBaseballers;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.stream.Stream;

public class Demo_5 {
    public static void main(String[] args) {
        BaseballersObserver observer = new BaseballersObserver();
        ObservableBaseballers observable = new ObservableBaseballers();
        observable.addObserver(observer);

        System.out.println("########################## OBSERVER PATTERN ##########################");
        observable.voegToe(BaseballerFactory.newFilledBaseballer("Mike Trout", LocalDate.of(1991, 8,7), Sex.MALE, "United States of America", "Los Angeles Angels", 27, "Center field", 0.305, 285));
        observable.verwijder("Los Angeles Angels", 27);

        System.out.println("########################## FACTORY PATTERN ##########################");
        System.out.println("Empty Baseballer:");
        System.out.println(BaseballerFactory.newEmptyBaseballer());

        System.out.println("\nFilled Baseballer:");
        System.out.println(BaseballerFactory.newFilledBaseballer("Jacob deGrom", LocalDate.of(1988, 6,16), Sex.MALE, "United States of America", "New York Mets", 48, "Pitcher", 0, 0));

        System.out.println("\n30 random baseballers gesorteerd op naam");
        Stream.generate(BaseballerFactory::newRandomBaseballer).limit(30).sorted(Comparator.comparing(Baseballer::getName)).forEach(System.out::println);

    }
}
