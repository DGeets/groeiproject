package be.kdg.baseballproject.model;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({TestBaseballer.class, TestBaseballers.class})
public class TestSuite {
}
