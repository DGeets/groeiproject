package be.kdg.baseballproject.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

import static org.junit.Assert.*;

public class TestBaseballers {
    private Baseballers baseballers;

    @Before
    public void setUp() throws Exception {
        baseballers = new Baseballers();
        baseballers.voegToe(new Baseballer("Mike Trout", LocalDate.of(1991, 8,7), Sex.MALE, "United States of America", "Los Angeles Angels", 27, "Center field", 0.305, 285));
    }

    @Test
    public void testVoegToe() {
        Assert.assertTrue("De methode \"voegToe\" van Baseballers werkt.", baseballers.voegToe(new Baseballer()));
    }

    @Test
    public void testVerwijder() {
        Assert.assertTrue("De methode \"verwijder\" van Baseballers werkt.", baseballers.verwijder("Los Angeles Angels", 27));
    }
}