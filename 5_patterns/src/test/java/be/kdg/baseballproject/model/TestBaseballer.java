package be.kdg.baseballproject.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class TestBaseballer {
    private Baseballer firstBaseballer;
    private Baseballer secondBaseballer;

    @Before
    public void setUp() throws Exception {
        firstBaseballer = new Baseballer();
        secondBaseballer = new Baseballer();
    }

    @Test
    public void testEquals() {
        boolean test = true;
        if (!firstBaseballer.equals(secondBaseballer)) {
            test = false;
        }
        firstBaseballer.setTeam("Best Team");
        if (firstBaseballer.equals(secondBaseballer)) {
            test = false;
        }
        Assert.assertTrue("De equals methode van baseballer werkt zoals verwacht", test);
    }

    @Test(expected = IllegalArgumentException.class )
    public void testIllegalHomeRuns() {
        firstBaseballer.setHomeRuns(-50);
        Assert.fail("De setter controleert goed of de homeruns niet negatief mag zijn");
    }

    @Test
    public void testLegalHomeRuns() {
        boolean test = true;
        try {
            firstBaseballer.setHomeRuns(50);
        } catch (IllegalArgumentException e){
            test = false;
        }
        Assert.assertTrue("Er treedt geen exception op bij een geldige waarde", test);
    }

    @Test
    public void testCompareTo() {
        boolean test = true;
        if (firstBaseballer.compareTo(secondBaseballer) != 0) {
            test = false;
        }
        Assert.assertTrue("Er treedt geen exception op bij een geldige waarde", test);
    }

    @Test
    public void testBattingAverage() {
        Assert.assertEquals("", 0, firstBaseballer.getBattingAverage(), 0.01);
    }
}