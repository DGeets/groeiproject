import model.Baseballer;
import model.Baseballers;
import model.Person;
import reflection.ReflectionTools;

public class Demo_3 {
    public static void main(String[] args) {
        ReflectionTools.classAnalysis(Baseballer.class, Baseballers.class, Person.class);
        System.out.println(ReflectionTools.runAnnotated(Baseballer.class));
    }
}
