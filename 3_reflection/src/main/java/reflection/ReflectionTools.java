package reflection;

import java.lang.invoke.MethodHandle;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectionTools {

    public static Object runAnnotated(Class aClass){
        try {
            Object newObject = aClass.newInstance();
            for (Method method : aClass.getDeclaredMethods()){
                if (method.isAnnotationPresent(CanRun.class) && method.getParameterCount() == 1 && method.getParameterTypes()[0] == String.class){
                    method.invoke(newObject, method.getAnnotation(CanRun.class).value());
                }
            }
            for (Method method : aClass.getSuperclass().getDeclaredMethods()){
                if (method.isAnnotationPresent(CanRun.class) && method.getParameterCount() == 1 && method.getParameterTypes()[0] == String.class){
                    method.invoke(newObject, method.getAnnotation(CanRun.class).value());
                }
            }
            return newObject;
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void classAnalysis(Class ...classes){
        for (int i = 0; i < classes.length; i++){
            System.out.println("Analyse van de klasse: " + classes[i].getSimpleName());
            System.out.println("----------------------------------------------------");

            System.out.println("Volledige klassenaam: " + classes[i].getName());

            System.out.println("Nama van de superklasse: " + classes[i].getSuperclass().getName());

            System.out.println("Naam van de package: " + classes[i].getPackage().getName());

            System.out.print("Interfaces: ");
            for (Class interf : classes[i].getInterfaces()) {
                System.out.print(interf.getName()+ " ");
            }

            System.out.print("\nConstructors: ");
            for (Constructor constr : classes[i].getDeclaredConstructors()) {
                System.out.print(constr+ " ");
            }

            System.out.print("\nPrivate attributen: ");
            for (Field field: classes[i].getDeclaredFields()) {
                System.out.print(field.getName() + " ");
            }

            System.out.print("\nGetters: ");
            for (Method method: classes[i].getDeclaredMethods()) {
                if (method.getName().startsWith("get")){
                    System.out.print(method.getName() + " ");
                }
            }

            System.out.print("\nSetters: ");
            for (Method method: classes[i].getDeclaredMethods()) {
                if (method.getName().startsWith("set")){
                    System.out.print(method.getName() + " ");
                }
            }

            System.out.print("\nAndere methoden: ");
            for (Method method: classes[i].getDeclaredMethods()) {
                if (!method.getName().startsWith("set") && !method.getName().startsWith("get")){
                    System.out.print(method.getName() + " ");
                }
            }
            System.out.println();
        }
    }
}
