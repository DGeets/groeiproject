package model;

import reflection.CanRun;

import java.time.LocalDate;

public class Person {
    //region attributes
    private String name;
    private LocalDate birthDay;
    private Sex sex;
    private String country;
    //endregion

    //region getters and setters
    public String getName() {
        return name;
    }

    @CanRun("Mike Trout")
    public void setName(String name) {
        if (name.isEmpty() || name == null) {
            throw new IllegalArgumentException("De naam mag niet null of leeg zijn");
        }
        this.name = name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    @CanRun()
    public void setBirthDay(LocalDate birthDay) {
        if (birthDay.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("De geboortedatum mag niet in de toekomst liggen");
        }
        this.birthDay = birthDay;
    }

    public Sex getSex() {
        return sex;
    }

    @CanRun()
    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    @CanRun("United States of America")
    public void setCountry(String country) {
        if (country.isEmpty() || country == null) {
            throw new IllegalArgumentException("De naam van het land mag niet leeg of null zijn");
        }
        this.country = country;
    }
    //endregion
}
