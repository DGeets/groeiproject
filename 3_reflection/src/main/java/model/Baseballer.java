package model;

import reflection.CanRun;

import java.time.LocalDate;

public class Baseballer extends Person implements Comparable<Baseballer> {

    //region constructors
    public Baseballer() {
        this("Ongekend", LocalDate.now().minusYears(50L), Sex.MALE, "Ongekend", "Ongekend", 0, "Ongekend", 0, 0);
    }

    public Baseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        setName(name);
        setBirthDay(birthDay);
        setSex(sex);
        setCountry(country);
        setTeam(team);
        setTeamNumber(teamNumber);
        setPosition(position);
        setBattingAverage(battingAverage);
        setHomeRuns(homeRuns);
    }
    //endregion

    //region attributes
    private String team;
    private int teamNumber;
    private String position;
    private double battingAverage;
    private int homeRuns;
    //endregion

    //region getters and setters
    public String getTeam() {
        return team;
    }

    @CanRun("New York Yankees")
    public void setTeam(String team) {
        if (team.isEmpty() || team == null) {
            throw new IllegalArgumentException("De naam van het team mag niet leeg of null zijn");
        }
        this.team = team;
    }

    public int getTeamNumber() {
        return teamNumber;
    }
    @CanRun()
    public void setTeamNumber(int teamNumber) {
        if (teamNumber > 99) {
            throw new IllegalArgumentException("De rugnummer van een speler kan niet boven 99 zijn");
        }
        this.teamNumber = teamNumber;
    }

    public String getPosition() {
        return position;
    }

    @CanRun("Pitcher")
    public void setPosition(String position) {
        if (position.isEmpty() || position == null) {
            throw new IllegalArgumentException("De positie van een speler mag niet leeg of null zijn");
        }
        this.position = position;
    }

    public double getBattingAverage() {
        return battingAverage;
    }

    @CanRun()
    public void setBattingAverage(double battingAverage) {
        if (battingAverage > 1 || battingAverage < 0) {
            throw new IllegalArgumentException("De batting average moet tussen 0 en 1 liggen");
        }
        this.battingAverage = battingAverage;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    @CanRun()
    public void setHomeRuns(int homeRuns) {
        if (homeRuns < 0) {
            throw new IllegalArgumentException("Het aantal homeruns kan niet lager zijn dan 0");
        }
        this.homeRuns = homeRuns;
    }
    //endregion

    //region override methods
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Baseballer)) return false;

        Baseballer baseballer = (Baseballer) obj;

        return baseballer.getTeamNumber() == this.getTeamNumber() && baseballer.getTeam().equals(this.getTeam());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + team.hashCode();
        result = 31 * result + teamNumber;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Name:%-20s\tBirthday:%-15s\tTeam:%-30s\tTeam number:%d\tBatting Avg:%1.3f\tHome runs:%d", this.getName(),this.getBirthDay(), this.getTeam(),this.getTeamNumber(), this.getBattingAverage(), this.getHomeRuns());
    }
    //endregion

    //region implement methods
    @Override
    public int compareTo(Baseballer baseballer) {
        int result = !(this.getTeam().compareTo(baseballer.getTeam()) == 0) ? this.getTeam().compareTo(baseballer.getTeam()) : this.getTeamNumber() - baseballer.getTeamNumber();
        return result;
    }
    //endregion

}
