import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.Sex;

import java.time.LocalDate;

public class Demo_1 {

    public static void main(String[] args){
        // 4.2 a
        Baseballers baseballers = new Baseballers();

        //4.2 b
        Data.getData().forEach(baseballers::voegToe);

        //4.2 c
        System.out.println("Toevoegen van duplicaat: " + baseballers.voegToe(Data.getData().get(5)));

        //4.2 d
        System.out.println(baseballers.zoek("Washington Nationals", 6));
        System.out.println(baseballers.verwijder("Washington Nationals", 6));
        System.out.println(baseballers.zoek("Washington Nationals", 6));
        System.out.println(baseballers.getAantal());

        //4.2 e
        System.out.println("OP NAAM");
        baseballers.gesorteerdOpNaam().forEach(System.out::println);

        System.out.println("OP GEBOORTE DAG");
        baseballers.gesorteerdopGeboorte().forEach(System.out::println);

        System.out.println("OP HOME RUNS");
        baseballers.gesorteerdOpHomeruns().forEach(System.out::println);

        //4.2 f
        Baseballer b = new Baseballer();
        System.out.println(b);
        System.out.println(new Baseballer("Christian Yelich", LocalDate.of(1991, 12,5), Sex.MALE, "United Stats of America", "Milwaukee Brewers", 22, "Center field", 0.301, 139));

        try{
            b.setHomeRuns(-50);
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setBattingAverage(2);
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setPosition("");
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setTeamNumber(100);
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setCountry("");
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setBirthDay(LocalDate.now().plusWeeks(2));
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }
        try{
            b.setName("");
        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
        }





    }
}
