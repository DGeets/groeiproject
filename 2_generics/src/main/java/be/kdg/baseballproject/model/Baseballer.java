package be.kdg.baseballproject.model;

import java.time.LocalDate;
/**
 * De klasse Baseballer beschrijft een professionele baseball speler. Deze klasse bevat enkele persoonlijke eigenschappen alsook statistieken over de spelers.
 * @author Dieter Geets
 * @version versie 2.0
 * @see <a href=https://en.wikipedia.org/wiki/Baseball"">Wikipedia over baseball</a>
 */
public class Baseballer implements Comparable<Baseballer> {

    //region constructors

    /**
     * Default constructor die alle waarden vult met dummy-waarden
     */
    public Baseballer() {
        this("Ongekend", LocalDate.now().minusYears(50L), Sex.MALE, "Ongekend", "Ongekend", 0, "Ongekend", 0, 0);
    }

    /**
     * Constructor voor alle attributen van de klasse Baseballer, worden allemaal opgevuld via de setter die controle uitvoert.
     * @param name
     * @param birthDay
     * @param sex
     * @param country
     * @param team
     * @param teamNumber
     * @param position
     * @param battingAverage
     * @param homeRuns
     */
    public Baseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        setName(name);
        setBirthDay(birthDay);
        setSex(sex);
        setCountry(country);
        setTeam(team);
        setTeamNumber(teamNumber);
        setPosition(position);
        setBattingAverage(battingAverage);
        setHomeRuns(homeRuns);
    }
    //endregion

    //region attributes
    /**
     * De naam van de baseballer
     */
    private String name;
    /**
     * De geboortedatum van de baseballer
     */
    private LocalDate birthDay;
    /**
     * Het geslacht van de baseballer
     * @see be.kdg.baseballproject.model.Sex
     */
    private Sex sex;
    /**
     * Het land van herkomst van de baseballer
     */
    private String country;
    /**
     * Het huidige team waarvoor de baseballer speelt
     */
    private String team;
    /**
     * De nummer van de baseballer binnen het huidige team
     */
    private int teamNumber;
    /**
     * De positie op het veld van de baseballer
     */
    private String position;
    /**
     * Gemiddelde score die de baseballer haalt, bij pitchers(positie) is dit altijd nul
     */
    private double battingAverage;
    /**
     * Aantal homeruns van de baseballer uit zijn cariere, bij pitchers(positie) is dit altijd nul
     */
    private int homeRuns;
    //endregion

    //region getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty() || name == null) {
            throw new IllegalArgumentException("De naam mag niet null of leeg zijn");
        }
        this.name = name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        if (birthDay.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("De geboortedatum mag niet in de toekomst liggen");
        }
        this.birthDay = birthDay;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (country.isEmpty() || country == null) {
            throw new IllegalArgumentException("De naam van het land mag niet leeg of null zijn");
        }
        this.country = country;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        if (team.isEmpty() || team == null) {
            throw new IllegalArgumentException("De naam van het team mag niet leeg of null zijn");
        }
        this.team = team;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        if (teamNumber > 99) {
            throw new IllegalArgumentException("De rugnummer van een speler kan niet boven 99 zijn");
        }
        this.teamNumber = teamNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        if (position.isEmpty() || position == null) {
            throw new IllegalArgumentException("De positie van een speler mag niet leeg of null zijn");
        }
        this.position = position;
    }

    public double getBattingAverage() {
        return battingAverage;
    }

    public void setBattingAverage(double battingAverage) {
        if (battingAverage > 1 || battingAverage < 0) {
            throw new IllegalArgumentException("De batting average moet tussen 0 en 1 liggen");
        }
        this.battingAverage = battingAverage;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(int homeRuns) {
        if (homeRuns < 0) {
            throw new IllegalArgumentException("Het aantal homeruns kan niet lager zijn dan 0");
        }
        this.homeRuns = homeRuns;
    }

    //endregion

    //region override methods

    /**
     * Bepaalt wat een baseballer uniek maakt, dit gebeurt op basis van team en nummer.
     * @param obj
     * @return boolean
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Baseballer)) return false;

        Baseballer baseballer = (Baseballer) obj;

        return baseballer.getTeamNumber() == this.getTeamNumber() && baseballer.getTeam().equals(this.getTeam());
    }

    /**
     * Bepaalt wat een baseballer uniek maakt, dit gebeurt op basis van team en nummer.
     * @param obj
     * @return boolean
     */
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + team.hashCode();
        result = 31 * result + teamNumber;
        return result;
    }

    /**
     * Maakt een overzicht van de baseballer om visueel te laten zien.
     * @return
     */
    @Override
    public String toString() {
        return String.format("Name:%-20s\tBirthday:%-15s\tTeam:%-30s\tTeam number:%d\tBatting Avg:%1.3f\tHome runs:%d", this.getName(),this.getBirthDay(), this.getTeam(),this.getTeamNumber(), this.getBattingAverage(), this.getHomeRuns());
    }
    //endregion

    //region implement methods

    /**
     * Bepaalt of twee baseballers dezelfde zijn, dit gebeurt op basis van team en nummer.
     * @param baseballer
     * @return
     */
    @Override
    public int compareTo(Baseballer baseballer) {
        int result = !(this.getTeam().compareTo(baseballer.getTeam()) == 0) ? this.getTeam().compareTo(baseballer.getTeam()) : this.getTeamNumber() - baseballer.getTeamNumber();
        return result;
    }
    //endregion

}
