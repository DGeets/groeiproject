package be.kdg.baseballproject.generics;

import java.util.*;

import static java.util.Collections.max;

/**
 * De klasse PriorityQueue is een generieke klasse die elementen behoudt op basis van een prioriteit.
 * @author Dieter Geets
 * @version versie 2.0
 * @param <T>
 */
public class PriorityQueue<T> implements FIFOQueue<T> {

    //region attributes
    private TreeMap<Integer, LinkedList<T>> list;
    //endregion

    //region constructors

    /**
     * Default constuctor die de Treemap attribuut initialiseert en de volgorde meteen omdraait.
     */
    public PriorityQueue() {
        list = new TreeMap<>(Comparator.reverseOrder());
        for (int i = 1; i < 6; i++) {
            list.put(i, new LinkedList<>());
        }
    }
    //endregion

    //region implement methods

    /**
     * Bijvullen van een element, met de parameter priority wordt bepaald in welke linkedlist dit element terecht komt.
     * @param element
     * @param priority
     * @return boolean
     */
    @Override
    public boolean enqueue(T element, int priority) {
        if (list.containsValue(element)) {
            return false;
        } else {
            list.get(priority).add(element);
            return true;
        }
    }

    /**
     * Deze methode geeft het element terug met de hoogste prioriteit, het eerste element van de eerste lijst met hoogste prioriteit die is gevuld.
     * @return element T
     */
    @Override
    public T dequeue() {
        for (LinkedList<T> treelist : list.values()) {
            if (treelist.size() > 0) {
                return treelist.removeFirst();
            }
        }
        return null;
    }

    /**
     * Geeft een int terug die zegt op welke plaats dit element zich bevindt in de prioriteit.
     * @param element
     * @return position
     */
    @Override
    public int search(T element) {
        int counter = 0;
        Set treeMapEntrySet = list.entrySet();
        Iterator iterator = treeMapEntrySet.iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry)iterator.next();
            ListIterator<T> listIterator = list.get(me.getKey()).listIterator();
            while (listIterator.hasNext()) {
                counter +=1;
                if (listIterator.next().equals(element)) {
                    return counter;
                }
            }
        }
        return -1;
    }

    /**
     * Geeft het aantal elementen terug die in de queue staan
     * @return aantal
     */
    @Override
    public int getSize() {
        int amount = 0;
        for (LinkedList<T> treelist : list.values()) {
            amount += treelist.size();
        }
        return amount;
    }
    //endregion

    //region override methods

    /**
     * Geeft een string van alle elementen in de queue, geordend volgens prioritiet.
     * @return string
     */
    @Override
    public String toString() {
        StringBuilder returnString = new StringBuilder();
        Set treeMapEntrySet = list.entrySet();
        Iterator iterator = treeMapEntrySet.iterator();
        while(iterator.hasNext()) {
            Map.Entry me = (Map.Entry)iterator.next();
            for (T element : list.get(me.getKey())) {
                returnString.append(me.getKey() + ": " + element + "\n");
            }
        }

        return returnString.toString();
    }
    //endregion
}
