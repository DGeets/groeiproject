import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.generics.PriorityQueue;
import be.kdg.baseballproject.model.Baseballer;

import java.util.Random;

public class Demo_2 {

    public static void main(String[] args){
        PriorityQueue<Baseballer> myQueue = new PriorityQueue<>();
        Random rnd = new Random();
        Data.getData().stream().forEach(baseballer -> myQueue.enqueue(baseballer, rnd.nextInt(5)+1));
        System.out.println("Overzicht van de PriorityQueue:");
        System.out.println(myQueue.toString());
        System.out.println("aantal: " + myQueue.getSize());
        System.out.println("positie van Mike Trout: " + myQueue.search(Data.getData().stream().findFirst().orElse(null)));
        for(int i = 0; i < 4; i++) {
            System.out.println("Dequeue: " + myQueue.dequeue());
        }
        System.out.println("Size na dequeue: " + myQueue.getSize());
    }
}
