package be.kdg.baseballproject.persist;

import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballers;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

public class BaseballerSerializerTest {
    private Baseballers baseballers;
    private BaseballerSerializer serializer;

    @Before
    public void setUp() throws Exception {
        serializer = new BaseballerSerializer();
        baseballers = new Baseballers();
        Data.getData().forEach(baseballers::voegToe);
    }

    @Test
    public void testSerialize() {
        boolean ok = true;
        try {
            serializer.serialize(baseballers);
        } catch (IOException e) {
            ok = false;
            e.printStackTrace();
        }
        Assert.assertTrue("Geen errors bij serialization" ,ok);
    }

    @Test
    public void testDeserialize() {
        boolean ok = true;
        try {
            Baseballers baseballerss = serializer.deserialize();
        } catch (IOException | ClassNotFoundException e) {
            ok = false;
            e.printStackTrace();
        }
        Assert.assertTrue("Geen errors bij serialization" ,ok);
    }
}