package be.kdg.baseballproject.persist;

import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import org.junit.*;

import java.nio.file.Paths;
import java.util.List;


public class BaseballerDbDaoTest {
     private static BaseballerDbDao db;

    @BeforeClass
    public static void setUpDB() throws Exception {
        db = BaseballerDbDao.getInstance(Paths.get("db/baseballersDb"));
    }

    @AfterClass
    public static void closeDB() throws Exception {
        db.close();
    }

    @Before
    public void setUp() throws Exception {
        Data.getData().forEach(db::insert);
    }

    @After
    public void tearDown() throws Exception {
        Data.getData().forEach(baseballer -> db.delete(baseballer.getTeam(), baseballer.getTeamNumber()));
    }

    @Test
    public void testInsert() {
        int amount = Data.getData().size();
        List<Baseballer>  list = db.sortedOn("SELECT * FROM baseballers");
        Assert.assertEquals( amount, list.size());
    }

    @Test
    public void testRetrieveUpdate() {
        Baseballer b = db.retrieve("Los Angeles Angels", 27);
        b.setTeam("test");
        db.update(b);
        Assert.assertEquals(b.getName(), db.retrieve("test", b.getTeamNumber()).getName());
        db.delete("test", b.getTeamNumber());
    }

    @Test
    public void testDelete() {
        Baseballer b = db.retrieve("Los Angeles Angels", 27);
        boolean ok = !db.delete(b.getTeam(), b.getTeamNumber());
        try {
            db.delete(b.getTeam(), b.getTeamNumber());
        } catch (Exception e) {
            ok = true;
        }
        Assert.assertTrue("Duplicaat kan niet verwijderd worden", ok);
    }

    @Test
    public void testSort() {
        Assert.assertEquals(Data.getData().size(), db.sortedOn("SELECT * FROM baseballers").size());
    }

    @Test
    public void testSingleton() {
        BaseballerDbDao newDb = BaseballerDbDao.getInstance(Paths.get("db/baseballersDb"));
        Assert.assertSame(db,db);
    }
}