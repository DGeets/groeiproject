package be.kdg.baseballproject.persist;

import be.kdg.baseballproject.model.Baseballer;

import java.util.List;

public interface BaseballerDao {
    boolean insert(Baseballer baseballer);
    boolean delete(String naam, int teamNumber);
    boolean update(Baseballer baseballer);
    Baseballer retrieve(String naam, int teamNumber);
    List<Baseballer> sortedOn(String query);

}
