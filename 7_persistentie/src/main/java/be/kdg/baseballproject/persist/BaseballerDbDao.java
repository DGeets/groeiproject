package be.kdg.baseballproject.persist;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Sex;

import java.nio.file.Path;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class BaseballerDbDao implements BaseballerDao {
    private Connection connection;
    private Path dataBasePath;
    private static BaseballerDbDao singleton;

    public BaseballerDbDao(Path databasePath) {
        if (connection == null) {
            this.dataBasePath = databasePath;
            try {
                this.connection = DriverManager.getConnection("jdbc:hsqldb:file:" + databasePath.toString(), "SA", "");
                maakTabel();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static BaseballerDbDao getInstance(Path dataBasePath) {
        if (singleton == null) {
            singleton = new BaseballerDbDao(dataBasePath);
        } else {
            if (singleton.dataBasePath != dataBasePath) {
                singleton = new BaseballerDbDao(dataBasePath);
            }
        }
        return singleton;
    }

    public void close() {
        if (connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                System.err.println("Fatal error: cannot close connection");
                System.exit(1);
            }
        }
    }

    private void maakTabel() {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            statement.execute("DROP TABLE baseballers IF EXISTS");
            statement.execute("CREATE TABLE baseballers (id INTEGER IDENTITY, naam VARCHAR(25), geslacht VARCHAR(15), geboortedatum DATE, battingAverage DOUBLE, homeRuns INTEGER, country VARCHAR(30), team VARCHAR(50), position VARCHAR(25), teamNumber INTEGER)");
        } catch (SQLException e) {
            System.err.println("Error: cannot create statement");
        } finally {
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean insert(Baseballer baseballer) {
        PreparedStatement prep = null;
        try {
            String sql = "INSERT INTO baseballers(id, naam, geslacht, geboortedatum, battingAverage, homeRuns, country, team, position, teamNumber) VALUES(NULL,?,?,?,?,?,?,?,?,?)";
            prep = connection.prepareStatement(sql);
            prep.setString(1, baseballer.getName());
            prep.setString(2, baseballer.getSex().name());
            prep.setDate(3, Date.valueOf(baseballer.getBirthDay()));
            prep.setDouble(4, baseballer.getBattingAverage());
            prep.setInt(5, baseballer.getHomeRuns());
            prep.setString(6, baseballer.getCountry());
            prep.setString(7, baseballer.getTeam());
            prep.setString(8, baseballer.getPosition());
            prep.setInt(9, baseballer.getTeamNumber());
            prep.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Error: cannot create baseballer");
            return false;
        } finally {
            try {
                if (prep != null) prep.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean delete(String naam, int teamNumber) {
        Statement statement = null;
        try {
            statement = connection.createStatement();
            boolean status = statement.execute(
                    "DELETE FROM baseballers WHERE team='" + naam + "' AND teamNumber='" + teamNumber + "'");
            return status;
        } catch (SQLException e) {
            System.err.println("Error: cannot delete baseballer");
            return false;
        } finally {
            try {
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean update(Baseballer baseballer) {
        PreparedStatement prep = null;
        try {
            String sql = "UPDATE baseballers SET naam = (?), geslacht = (?), geboortedatum = (?), battingAverage = (?), homeRuns = (?), country = (?), team = (?), position = (?), teamNumber = (?) WHERE id= (?)";
            prep = connection.prepareStatement(sql);
            prep.setString(1, baseballer.getName());
            prep.setString(2, baseballer.getSex().name());
            prep.setDate(3, Date.valueOf(baseballer.getBirthDay()));
            prep.setDouble(4, baseballer.getBattingAverage());
            prep.setInt(5, baseballer.getHomeRuns());
            prep.setString(6, baseballer.getCountry());
            prep.setString(7, baseballer.getTeam());
            prep.setString(8, baseballer.getPosition());
            prep.setInt(9, baseballer.getTeamNumber());
            prep.setInt(10, baseballer.getId());
            prep.executeUpdate();
            return true;
        } catch (SQLException e) {
            System.err.println("Error: cannot update soldaat");
            return false;
        } finally {
            try {
                if (prep != null) prep.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public Baseballer retrieve(String team, int teamNumber) {
        Statement statement = null;
        ResultSet resultSet = null;
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(
                    "SELECT * FROM baseballers WHERE team='" + team + "' AND teamNumber='" + teamNumber+ "'");
            if (resultSet.next()) {
                Baseballer baseballer;
                baseballer = new Baseballer(
                        resultSet.getInt("id"),
                        resultSet.getString("naam"),
                        resultSet.getDate("geboortedatum").toLocalDate(),
                        Sex.valueOf(resultSet.getString("geslacht")),
                        resultSet.getString("country"),
                        resultSet.getString("team"),
                        resultSet.getInt("teamNumber"),
                        resultSet.getString("position"),
                        resultSet.getDouble("battingAverage"),
                        resultSet.getInt("Homeruns"));
                return baseballer;
            }
            return null;
        } catch (SQLException e) {
            System.err.println("Cannot execute statement");
            return null;
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public List<Baseballer> sortedOn(String query) {
        Statement statement = null;
        ResultSet resultSet = null;
        List<Baseballer> baseballerList = new ArrayList<>();
        try {
            statement = connection.createStatement();
            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                baseballerList.add(new Baseballer(
                        resultSet.getInt("id"),
                        resultSet.getString("naam"),
                        resultSet.getDate("geboortedatum").toLocalDate(),
                        Sex.valueOf(resultSet.getString("geslacht")),
                        resultSet.getString("country"),
                        resultSet.getString("team"),
                        resultSet.getInt("teamNumber"),
                        resultSet.getString("position"),
                        resultSet.getDouble("battingAverage"),
                        resultSet.getInt("Homeruns")));
            }
        } catch (
                SQLException e) {
            System.err.println("Cannot execute statement");
            return null;
        } finally {
            try {
                if (resultSet != null) resultSet.close();
                if (statement != null) statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return baseballerList;
    }
}
