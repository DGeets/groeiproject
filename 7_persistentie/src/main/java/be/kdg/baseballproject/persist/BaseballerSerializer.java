package be.kdg.baseballproject.persist;

import be.kdg.baseballproject.model.Baseballers;

import java.io.*;

public class BaseballerSerializer {
    private final String BASE_URL = "db/baseballers.ser";

    public void serialize(Baseballers baseballers) throws IOException {
        try (FileOutputStream fileOutputStream = new FileOutputStream(BASE_URL)) {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(baseballers);
            System.out.println("Opgeslagen in " + BASE_URL);
        }
    }

    public Baseballers deserialize() throws IOException, ClassNotFoundException {
        try (FileInputStream fileInputStream = new FileInputStream(BASE_URL)) {
            ObjectInputStream objectOutputStream = new ObjectInputStream(fileInputStream);
            return (Baseballers) objectOutputStream.readObject();
        }
    }
}
