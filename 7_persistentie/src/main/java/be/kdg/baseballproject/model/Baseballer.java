package be.kdg.baseballproject.model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Comparator;

public class Baseballer implements Comparable<Baseballer>, Serializable {
    //region constructors
    public Baseballer() {
        this("Ongekend", LocalDate.now().minusYears(50L), Sex.MALE, "Ongekend", "Ongekend", 0, "Ongekend", 0, 0);
    }

    public Baseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        this(-1, name, birthDay, sex, country, team, teamNumber, position, battingAverage, homeRuns);
    }

    public Baseballer(int id, String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        this.id = id;
        setName(name);
        setBirthDay(birthDay);
        setSex(sex);
        setCountry(country);
        setTeam(team);
        setTeamNumber(teamNumber);
        setPosition(position);
        setBattingAverage(battingAverage);
        setHomeRuns(homeRuns);
    }
    //endregion

    //region attributes
    private int id;
    private String name;
    private LocalDate birthDay;
    private transient Sex sex;
    private transient String country;
    private String team;
    private int teamNumber;
    private transient String position;
    private double battingAverage;
    private int homeRuns;
    private static final long serialVersionUID = 1L;
    //endregion

    //region getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty() || name == null) {
            throw new IllegalArgumentException("De naam mag niet null of leeg zijn");
        }
        this.name = name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        if (birthDay.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("De geboortedatum mag niet in de toekomst liggen");
        }
        this.birthDay = birthDay;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (country.isEmpty() || country == null) {
            throw new IllegalArgumentException("De naam van het land mag niet leeg of null zijn");
        }
        this.country = country;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        if (team.isEmpty() || team == null) {
            throw new IllegalArgumentException("De naam van het team mag niet leeg of null zijn");
        }
        this.team = team;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        if (teamNumber > 99) {
            throw new IllegalArgumentException("De rugnummer van een speler kan niet boven 99 zijn");
        }
        this.teamNumber = teamNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        if (position.isEmpty() || position == null) {
            throw new IllegalArgumentException("De positie van een speler mag niet leeg of null zijn");
        }
        this.position = position;
    }

    public double getBattingAverage() {
        return battingAverage;
    }

    public void setBattingAverage(double battingAverage) {
        if (battingAverage > 1 || battingAverage < 0) {
            throw new IllegalArgumentException("De batting average moet tussen 0 en 1 liggen");
        }
        this.battingAverage = battingAverage;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(int homeRuns) {
        if (homeRuns < 0) {
            throw new IllegalArgumentException("Het aantal homeruns kan niet lager zijn dan 0");
        }
        this.homeRuns = homeRuns;
    }

    //endregion

    //region override methods
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Baseballer)) return false;

        Baseballer baseballer = (Baseballer) obj;

        return baseballer.getTeamNumber() == this.getTeamNumber() && baseballer.getTeam().equals(this.getTeam());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + team.hashCode();
        result = 31 * result + teamNumber;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Name:%-20s\tBirthday:%-15s\tTeam:%-30s\tTeam number:%d\tBatting Avg:%1.3f\tHome runs:%d", this.getName(),this.getBirthDay(), this.getTeam(),this.getTeamNumber(), this.getBattingAverage(), this.getHomeRuns());
    }
    //endregion

    //region implement methods
    @Override
    public int compareTo(Baseballer baseballer) {
        int result = !(this.getTeam().compareTo(baseballer.getTeam()) == 0) ? this.getTeam().compareTo(baseballer.getTeam()) : this.getTeamNumber() - baseballer.getTeamNumber();
        return result;
    }
    //endregion

}
