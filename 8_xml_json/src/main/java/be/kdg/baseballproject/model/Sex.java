package be.kdg.baseballproject.model;

public enum Sex {
    MALE, FEMALE
}
