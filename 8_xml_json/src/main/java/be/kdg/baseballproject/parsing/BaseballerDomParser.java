package be.kdg.baseballproject.parsing;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.Sex;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;

public class BaseballerDomParser {

    public static Baseballers domReadXml(String filename) {
        Baseballers baseballers = new Baseballers();
        try {
            Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new File(filename));
            Element rootElement = doc.getDocumentElement();
            NodeList nodeList = rootElement.getChildNodes();

            for (int i = 0; i < nodeList.getLength(); i++) {
                if (nodeList.item(i).getNodeType() != Node.ELEMENT_NODE) {
                    continue;
                }
                Element e = (Element) nodeList.item(i);
                baseballers.voegToe(new Baseballer(
                        e.getElementsByTagName("name").item(0).getTextContent(),
                        LocalDate.parse(e.getAttribute("birthday")),
                        Sex.valueOf(e.getElementsByTagName("sex").item(0).getTextContent()),
                        e.getElementsByTagName("country").item(0).getTextContent(),
                        e.getElementsByTagName("team").item(0).getTextContent(),
                        Integer.parseInt(e.getElementsByTagName("teamNummer").item(0).getTextContent()),
                        e.getElementsByTagName("position").item(0).getTextContent(),
                        Double.parseDouble(e.getElementsByTagName("battingAverage").item(0).getTextContent()),
                        Integer.parseInt(e.getElementsByTagName("homeRuns").item(0).getTextContent())
                ));
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return baseballers;
    }
}
