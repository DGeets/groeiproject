package be.kdg.baseballproject.parsing;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;

public class BaseballerStaxParser {
    private Baseballers baseballers;
    private String writePath;
    private XMLStreamWriter streamWriter;

    public BaseballerStaxParser(Baseballers baseballers, String writePath) {
        this.baseballers = baseballers;
        this.writePath = writePath;

        try {
            streamWriter = new IndentingXMLStreamWriter(XMLOutputFactory.newInstance().createXMLStreamWriter(new FileWriter(writePath)));
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void writeXml() {
        try {
            streamWriter.writeStartDocument();
            streamWriter.writeStartElement("Baseballers");
            for (Baseballer b: baseballers.gesorteerdOpNaam()) {
                streamWriter.writeStartElement("Baseballer");
                streamWriter.writeAttribute("birthday", b.getBirthDay().toString());
                writeElement(b);
                streamWriter.writeEndElement();
            }
            streamWriter.writeEndElement();
            streamWriter.close();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }
    }

    private void writeElement(Baseballer b) {
        try {
            streamWriter.writeStartElement("name");
            streamWriter.writeCharacters(b.getName());
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("sex");
            streamWriter.writeCharacters(b.getSex().toString());
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("country");
            streamWriter.writeCharacters(b.getCountry());
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("team");
            streamWriter.writeCharacters(b.getTeam());
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("teamNummer");
            streamWriter.writeCharacters(String.valueOf(b.getTeamNumber()));
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("position");
            streamWriter.writeCharacters(b.getPosition());
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("battingAverage");
            streamWriter.writeCharacters(String.valueOf(b.getBattingAverage()));
            streamWriter.writeEndElement();

            streamWriter.writeStartElement("homeRuns");
            streamWriter.writeCharacters(String.valueOf(b.getHomeRuns()));
            streamWriter.writeEndElement();
        } catch (XMLStreamException e) {
            e.printStackTrace();
        }

    }
}
