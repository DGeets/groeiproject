package be.kdg.baseballproject.parsing;

import be.kdg.baseballproject.model.Baseballer;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;

public class BaseballerJaxbParser {

    public static void JaxbWriteXml(String file, Object root) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(root.getClass());
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(root, new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }

    public static <T> T JaxbReadXml(String file, Class<T> typeParameterClass) {
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(typeParameterClass);
            Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

            return (T) unmarshaller.unmarshal(new File(file));
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
}
