package be.kdg.baseballproject.parsing;

import be.kdg.baseballproject.model.Baseballers;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.*;

public class BaseballerGsonParser {

    public static void writeJson(Baseballers baseballers, String fileName) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String jsonString = gson.toJson(baseballers);
        try (FileWriter jsonwriter = new FileWriter(fileName)) {
            jsonwriter.write(jsonString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Baseballers readJson(String fileName) {
        Baseballers baseballers = null;
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        try (BufferedReader data = new BufferedReader(new FileReader(fileName))) {
            baseballers = gson.fromJson(data, Baseballers.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baseballers;
    }
}
