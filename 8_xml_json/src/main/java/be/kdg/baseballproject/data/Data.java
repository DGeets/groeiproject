package be.kdg.baseballproject.data;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Sex;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Data {

    public static List<Baseballer> getData(){
        List<Baseballer> baseballers = new ArrayList<>();

        Baseballer b1 = new Baseballer("Mike Trout", LocalDate.of(1991, 8,7), Sex.MALE, "United States of America", "Los Angeles Angels", 27, "Center field", 0.305, 285);
        baseballers.add(b1);

        Baseballer b2 = new Baseballer("Mookie Betts", LocalDate.of(1992, 10,7), Sex.MALE, "United States of America", "Boston Red Sox", 50, "Right field", 0.301, 139);
        baseballers.add(b2);

        Baseballer b3 = new Baseballer("Nolan Arenado", LocalDate.of(1991, 4,16), Sex.MALE, "United States of America", "Colorado Rockies", 28, "Third baseman", 0.295, 227);
        baseballers.add(b3);

        Baseballer b4 = new Baseballer("Francisco Lindor", LocalDate.of(1993, 11,14), Sex.MALE, "Puerto Rico", "Cleveland Indians", 12, "Shortstop", 0.288, 129);
        baseballers.add(b4);

        Baseballer b5 = new Baseballer("Max Scherzer", LocalDate.of(1984, 7,27), Sex.MALE, "United States of America", "Washington Nationals", 31, "Pitcher", 0, 0);
        baseballers.add(b5);

        Baseballer b6 = new Baseballer("Jose Altuve", LocalDate.of(1990, 5,6), Sex.MALE, "Venezuela", "Houston Atros", 27, "Second baseman", 0.315, 128);
        baseballers.add(b6);

        Baseballer b7 = new Baseballer("Alex bregman", LocalDate.of(1994, 3,30), Sex.MALE, "United States of America", "Houston Atros", 2, "Third baseman", 0.286, 99);
        baseballers.add(b7);

        Baseballer b8 = new Baseballer("Christian Yelich", LocalDate.of(1991, 12,5), Sex.MALE, "United States of America", "Milwaukee Brewers", 22, "Center field", 0.301, 139);
        baseballers.add(b8);

        Baseballer b9 = new Baseballer("Jose Ramirez", LocalDate.of(1992, 9,17), Sex.MALE, "Dominican Republic", "Cleveland Indians", 11, "Third baseman", 0.280, 110);
        baseballers.add(b9);

        Baseballer b10 = new Baseballer("Jacob deGrom", LocalDate.of(1988, 6,16), Sex.MALE, "United States of America", "New York Mets", 48, "Pitcher", 0, 0);
        baseballers.add(b10);

        Baseballer b11 = new Baseballer("Anthony Rendon", LocalDate.of(1990, 6,6), Sex.MALE, "United States of America", "Washington Nationals", 6, "Third baseman", 0.291, 136);
        baseballers.add(b11);

        Baseballer b12 = new Baseballer("Trevor Bauer", LocalDate.of(1991, 1,17), Sex.MALE, "United States of America", "Cincinnati Reds", 27, "Pitcher", 0, 0);
        baseballers.add(b12);

        Baseballer b13 = new Baseballer("George Springer", LocalDate.of(1989, 9,19), Sex.MALE, "United States of America", "Houston Atros", 4, "Outfield", 0.269, 159);
        baseballers.add(b13);

        Baseballer b14 = new Baseballer("Blake Snell", LocalDate.of(1992, 12,4), Sex.MALE, "United States of America", "Tampa Bay Rays", 4, "Pitcher", 0, 0);
        baseballers.add(b14);

        Baseballer b15 = new Baseballer("Melissa Mayeux", LocalDate.of(1998, 10,2), Sex.FEMALE, "France", "Montpellier Universite Club", 13, "Shortstop", 0.343, 24);
        baseballers.add(b15);

        return baseballers;
    }
}
