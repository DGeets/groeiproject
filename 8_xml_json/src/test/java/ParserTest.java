import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.parsing.BaseballerDomParser;
import be.kdg.baseballproject.parsing.BaseballerGsonParser;
import be.kdg.baseballproject.parsing.BaseballerJaxbParser;
import be.kdg.baseballproject.parsing.BaseballerStaxParser;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class ParserTest {
    private Baseballers baseballers;

    @Before
    public void setUp() throws Exception {
        baseballers = new Baseballers();
        Data.getData().forEach(baseballers::voegToe);
    }

    @Test
    public void testStaxDom() {
        BaseballerStaxParser staxParser = new BaseballerStaxParser(baseballers, "datafiles/baseballers.xml");
        staxParser.writeXml();

        Baseballers baseballersTest = BaseballerDomParser.domReadXml("datafiles/baseballers.xml");
        boolean ok = true;
        List<Baseballer> list1 = baseballers.gesorteerdOpNaam();
        List<Baseballer> list2 = baseballersTest.gesorteerdOpNaam();
        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equals(list2.get(i))) ok = false;
        }
        Assert.assertTrue(ok);
    }

    @Test
    public void testJaxb() {
        BaseballerJaxbParser.JaxbWriteXml("datafiles/Jaxbbaseballers.xml", baseballers);

        Baseballers baseballersTest = BaseballerJaxbParser.JaxbReadXml("datafiles/Jaxbbaseballers.xml", Baseballers.class);

        boolean ok = true;
        List<Baseballer> list1 = baseballers.gesorteerdOpNaam();
        List<Baseballer> list2 = baseballersTest.gesorteerdOpNaam();
        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equals(list2.get(i))) ok = false;
        }
        Assert.assertTrue(ok);
    }

    @Test
    public void testGson() {
        BaseballerGsonParser.writeJson(baseballers, "datafiles/baseballers.json");

        Baseballers baseballersTest = BaseballerGsonParser.readJson("datafiles/baseballers.json");

        boolean ok = true;
        List<Baseballer> list1 = baseballers.gesorteerdOpNaam();
        List<Baseballer> list2 = baseballersTest.gesorteerdOpNaam();
        for (int i = 0; i < list1.size(); i++) {
            if (!list1.get(i).equals(list2.get(i))) ok = false;
        }
        Assert.assertTrue(ok);
    }
}
