import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.Sex;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.time.LocalDate;
import java.util.logging.LogManager;

public class Demo_6 {

    public static void main(String[] args){
        URL configURL = Demo_6.class.getResource("logging.properties");
        if (configURL != null) {
            try (InputStream is = configURL.openStream()) { LogManager.getLogManager().readConfiguration(is);
            } catch (IOException e) { System.err.println("Configuratiebestand is corrupt");
            }
        } else {
            System.err.println("Configuratiebestand NIET GEVONDEN");
        }
        Baseballer baseballer = Data.getData().get(0);
        Baseballers baseballers = new Baseballers();
        baseballer.setBattingAverage(2);
        baseballer.setHomeRuns(-2);
        baseballers.voegToe(baseballer);
        baseballers.verwijder(baseballer.getTeam(), baseballer.getTeamNumber());

    }
}
