package be.kdg.baseballproject.model;

import java.time.LocalDate;
import java.util.Comparator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Baseballer implements Comparable<Baseballer> {

    //region constructors
    public Baseballer() {
        this("Ongekend", LocalDate.now().minusYears(50L), Sex.MALE, "Ongekend", "Ongekend", 0, "Ongekend", 0, 0);
    }

    public Baseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        setName(name);
        setBirthDay(birthDay);
        setSex(sex);
        setCountry(country);
        setTeam(team);
        setTeamNumber(teamNumber);
        setPosition(position);
        setBattingAverage(battingAverage);
        setHomeRuns(homeRuns);
        logger = Logger.getLogger("be.kdg.baseballproject.model.Baseballer");
    }
    //endregion

    //region attributes
    private String name;
    private LocalDate birthDay;
    private Sex sex;
    private String country;
    private String team;
    private int teamNumber;
    private String position;
    private double battingAverage;
    private int homeRuns;
    private Logger logger;
    //endregion

    //region getters and setters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        if (name.isEmpty() || name == null) {
            logger.log(Level.SEVERE, name + " mag niet nul zijn van " + this.name);
        }
        this.name = name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(LocalDate birthDay) {
        if (birthDay.isAfter(LocalDate.now())) {
            logger.log(Level.SEVERE, birthDay + " mag niet in de toekomst liggen van " + this.name);
        }
        this.birthDay = birthDay;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        if (country.isEmpty() || country == null) {
            logger.log(Level.SEVERE, country + " mag niet nul zijn van " + this.name);
        }
        this.country = country;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        if (team.isEmpty() || team == null) {
            logger.log(Level.SEVERE, team + " mag niet nul zijn van " + this.name);
        }
        this.team = team;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public void setTeamNumber(int teamNumber) {
        if (teamNumber > 99) {
            logger.log(Level.SEVERE, teamNumber + " mag niet groter zijn dan 100 van " + this.name);
        }
        this.teamNumber = teamNumber;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        if (position.isEmpty() || position == null) {
            logger.log(Level.SEVERE, position + " mag niet nul zijn van " + this.name);
        }
        this.position = position;
    }

    public double getBattingAverage() {
        return battingAverage;
    }

    public void setBattingAverage(double battingAverage) {
        if (battingAverage > 1 || battingAverage < 0) {
            logger.log(Level.SEVERE, battingAverage + " moet tussen 0 en 1 liggen van " + this.name);
        }
        this.battingAverage = battingAverage;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    public void setHomeRuns(int homeRuns) {
        if (homeRuns < 0) {
            logger.log(Level.SEVERE, homeRuns + " mag niet negatief zijn van " + this.name);
        }
        this.homeRuns = homeRuns;
    }

    //endregion

    //region override methods
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Baseballer)) return false;

        Baseballer baseballer = (Baseballer) obj;

        return baseballer.getTeamNumber() == this.getTeamNumber() && baseballer.getTeam().equals(this.getTeam());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + team.hashCode();
        result = 31 * result + teamNumber;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Name:%-20s\tBirthday:%-15s\tTeam:%-30s\tTeam number:%d\tBatting Avg:%1.3f\tHome runs:%d", this.getName(), this.getBirthDay(), this.getTeam(), this.getTeamNumber(), this.getBattingAverage(), this.getHomeRuns());
    }
    //endregion

    //region implement methods
    @Override
    public int compareTo(Baseballer baseballer) {
        int result = !(this.getTeam().compareTo(baseballer.getTeam()) == 0) ? this.getTeam().compareTo(baseballer.getTeam()) : this.getTeamNumber() - baseballer.getTeamNumber();
        return result;
    }
    //endregion

}
