import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.BaseballerFactory;
import be.kdg.baseballproject.model.Sex;
import be.kdg.baseballproject.threading.BaseballerAttacker;

import java.util.ArrayList;
import java.util.List;

public class Demo_11 {

    public static void main(String[] args) {
        List<Baseballer> list = new ArrayList<>();

        for (int i = 0; i < 1000; i++) {
            list.add(BaseballerFactory.newRandomBaseballer());
        }


        BaseballerAttacker attack1 = new BaseballerAttacker(list, b -> b.getSex() == Sex.MALE);
        BaseballerAttacker attack2 = new BaseballerAttacker(list, b -> b.getTeamNumber() > 50);
        BaseballerAttacker attack3 = new BaseballerAttacker(list, b -> b.getHomeRuns() < 50);

        Thread thread1 = new Thread(attack1);
        Thread thread2 = new Thread(attack2);
        Thread thread3 = new Thread(attack3);

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("Na uitzuivering: ");
        System.out.printf("Aantal mannen: %d", attack1.getList().size());
        System.out.println();
        System.out.printf("Aantal baseballers met een nummer boven 50: %d", attack2.getList().size());
        System.out.println();
        System.out.printf("Aantal baseballers met minder dan 50 homeruns: %d",attack3.getList().size());

    }

}
