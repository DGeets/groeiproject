package be.kdg.baseballproject.model;

import java.time.LocalDate;
import java.util.Comparator;

public final class Baseballer implements Comparable<Baseballer> {

    //region constructors
    public Baseballer() {
        this("Ongekend", LocalDate.now().minusYears(50L), Sex.MALE, "Ongekend", "Ongekend", 0, "Ongekend", 0, 0);
    }

    public Baseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        this.name = name;
        this.birthDay = birthDay;
        this.sex = Sex.valueOf(sex.name());
        this.country = country;
        this.team = team;
        this.teamNumber = teamNumber;
        this.position = position;
        this.battingAverage = battingAverage;
        this.homeRuns = homeRuns;
    }
    //endregion

    //region attributes
    private final String name;
    private final LocalDate birthDay;
    private final Sex sex;
    private final String country;
    private final String team;
    private final int teamNumber;
    private final String position;
    private final double battingAverage;
    private final int homeRuns;
    //endregion

    //region getters and setters

    public String getName() {
        return name;
    }

    public LocalDate getBirthDay() {
        return birthDay;
    }

    public Sex getSex() {
        return Sex.valueOf(sex.name());
    }

    public String getCountry() {
        return country;
    }

    public String getTeam() {
        return team;
    }

    public int getTeamNumber() {
        return teamNumber;
    }

    public String getPosition() {
        return position;
    }

    public double getBattingAverage() {
        return battingAverage;
    }

    public int getHomeRuns() {
        return homeRuns;
    }

    //endregion

    //region override methods
    @Override
    public boolean equals(Object obj) {
        if (obj == this) return true;
        if (!(obj instanceof Baseballer)) return false;

        Baseballer baseballer = (Baseballer) obj;

        return baseballer.getTeamNumber() == this.getTeamNumber() && baseballer.getTeam().equals(this.getTeam());
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + team.hashCode();
        result = 31 * result + teamNumber;
        return result;
    }

    @Override
    public String toString() {
        return String.format("Name:%-20s\tBirthday:%-15s\tTeam:%-30s\tTeam number:%d\tBatting Avg:%1.3f\tHome runs:%d", this.getName(),this.getBirthDay(), this.getTeam(),this.getTeamNumber(), this.getBattingAverage(), this.getHomeRuns());
    }
    //endregion

    //region implement methods
    @Override
    public int compareTo(Baseballer baseballer) {
        int result = !(this.getTeam().compareTo(baseballer.getTeam()) == 0) ? this.getTeam().compareTo(baseballer.getTeam()) : this.getTeamNumber() - baseballer.getTeamNumber();
        return result;
    }
    //endregion

}
