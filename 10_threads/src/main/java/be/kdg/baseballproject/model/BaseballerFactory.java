package be.kdg.baseballproject.model;

import java.time.LocalDate;
import java.util.Random;

public class BaseballerFactory {

    public static Baseballer newEmptyBaseballer() {
        return new Baseballer();
    }

    public static Baseballer newFilledBaseballer(String name, LocalDate birthDay, Sex sex, String country, String team, int teamNumber, String position, double battingAverage, int homeRuns) {
        return new Baseballer(name, birthDay, sex, country, team, teamNumber, position, battingAverage, homeRuns);
    }

    public static Baseballer newRandomBaseballer() {
        Random rnd = new Random();
        String name = generateString(rnd.nextInt(10)+1, 2, true);
        LocalDate birthDay = LocalDate.of(rnd.nextInt(100) + 1900, rnd.nextInt(11)+1, rnd.nextInt(27)+1);
        Sex sex;
        if (rnd.nextBoolean()) {
            sex = Sex.MALE;
        } else {
            sex = Sex.FEMALE;
        }
        String country = generateString(rnd.nextInt(10)+1, 1, true);
        String team = generateString(rnd.nextInt(10)+1, 1, true);
        int teamNumber = rnd.nextInt(99)+1;
        String position = generateString(rnd.nextInt(10)+1, 1, true);
        double battingAverage = 0.35 * rnd.nextDouble();
        int homeRuns = rnd.nextInt(365);
        return new Baseballer(name, birthDay, sex, country, team, teamNumber, position, battingAverage, homeRuns);
    }

    private static String generateString(int maxWordLength, int wordCount, boolean camelCase) {
        StringBuilder str = new StringBuilder();
        Random rnd = new Random();
        for (int i = 0; i < wordCount; i++) {
            int random = rnd.nextInt(maxWordLength)+4;
            for (int j = 0; j < random; j++) {
                if (rnd.nextInt(3 + 1) == 1) {
                    char[] klinkers = {'a', 'e', 'i', 'o', 'u', 'y'};
                    String klinker = String.valueOf(klinkers[rnd.nextInt(klinkers.length)]);
                    if (camelCase && j == 0){
                        klinker = klinker.toUpperCase();
                    }
                    str.append(klinker);
                } else {
                    char[] medeklinkers = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'z'};
                    String medeklinker = String.valueOf(medeklinkers[rnd.nextInt(medeklinkers.length)]);
                    if (j == 0 && camelCase){
                        medeklinker = medeklinker.toUpperCase();
                    }
                    str.append(medeklinker);
                }
            }
            str.append(" ");
        }
        return str.toString();
    }
}
