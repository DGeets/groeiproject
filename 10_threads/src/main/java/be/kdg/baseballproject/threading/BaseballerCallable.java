package be.kdg.baseballproject.threading;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.BaseballerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseballerCallable implements Callable {
    private Predicate<Baseballer> predicate;
    private List<Baseballer> baseballers;
    private long time;

    public BaseballerCallable(Predicate<Baseballer> predicate) {
        this.predicate = predicate;
        baseballers = new ArrayList<>();
    }

    @Override
    public Object call() throws Exception {
        long now = System.currentTimeMillis();
        baseballers =  Stream.generate(BaseballerFactory::newRandomBaseballer).filter(predicate).limit(1000).collect(Collectors.toList());
        time = System.currentTimeMillis() - now;
        return baseballers;
    }

    public long getTime() {
        return time;
    }
}
