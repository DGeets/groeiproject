package be.kdg.baseballproject.threading;

import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.BaseballerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class BaseballerRunnable implements Runnable {

    private Predicate<Baseballer> predicate;
    private List<Baseballer> baseballers;
    private long time = 0;

    public BaseballerRunnable(Predicate<Baseballer> predicate) {
        baseballers = new ArrayList<>();
        this.predicate = predicate;
    }

    @Override
    public void run() {
        long start = System.currentTimeMillis();
        baseballers = Stream.generate(BaseballerFactory::newRandomBaseballer).filter(predicate).limit(1000).collect(Collectors.toList());
        time = System.currentTimeMillis() - start;
    }

    public List<Baseballer> getBaseballers() {
        return baseballers;
    }

    public long getTime() {
        return time;
    }
}
