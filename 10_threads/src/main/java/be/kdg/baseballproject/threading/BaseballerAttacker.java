package be.kdg.baseballproject.threading;

import be.kdg.baseballproject.model.Baseballer;

import java.util.List;
import java.util.function.Predicate;

public class BaseballerAttacker  implements Runnable{

    private List<Baseballer> list;
    private Predicate<Baseballer> predicate;

    public BaseballerAttacker(List<Baseballer> list, Predicate<Baseballer> predicate) {
        this.list = list;
        this.predicate = predicate;
    }

    @Override
    public void run() {
        synchronized (list) {
            list.removeIf(predicate);
        }
    }

    public List<Baseballer> getList() {
        return list;
    }
}
