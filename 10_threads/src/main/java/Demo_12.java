import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Sex;
import be.kdg.baseballproject.threading.BaseballerCallable;
import be.kdg.baseballproject.threading.BaseballerRunnable;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class Demo_12 {
    public static void main(String[] args) {
        BaseballerCallable callable1 = new BaseballerCallable(baseballer -> baseballer.getSex() == Sex.FEMALE);
        BaseballerCallable callable2 = new BaseballerCallable(baseballer -> baseballer.getSex() == Sex.MALE);
        BaseballerCallable callable3 = new BaseballerCallable(baseballer -> baseballer.getHomeRuns() < 50);

        long time = 0;
        int TESTCOUNT = 100;

        for (int i = 0; i < TESTCOUNT; i++) {
            ExecutorService pool = Executors.newFixedThreadPool(3);
            Set<Future<List<Baseballer>>> set = new HashSet<>();

            Future<List<Baseballer>> future1 = pool.submit(callable1);
            Future<List<Baseballer>> future2 = pool.submit(callable2);
            Future<List<Baseballer>> future3 = pool.submit(callable3);

            set.add(future1);
            set.add(future2);
            set.add(future3);

            pool.shutdown();

            for (Future<List<Baseballer>> list : set) {
                try {
                    List<Baseballer> baseballerList = list.get();
                    long average = callable1.getTime() + callable2.getTime() + callable3.getTime();
                    average /= 3;
                    time += average;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } catch (ExecutionException e) {
                    e.printStackTrace();
                }
            }

        }

        System.out.println("3 Futures verzamelen elk 1000 baseballers (gemiddelde uit 100 runs): " + time + "ms");
    }
}
