import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.Sex;
import be.kdg.baseballproject.threading.BaseballerRunnable;

import java.time.LocalDate;

public class Demo_10 {

    public static void main(String[] args){
        BaseballerRunnable run1 = new BaseballerRunnable(baseballer -> baseballer.getSex() == Sex.FEMALE);
        BaseballerRunnable run2 = new BaseballerRunnable(baseballer -> baseballer.getSex() == Sex.MALE);
        BaseballerRunnable run3 = new BaseballerRunnable(baseballer -> baseballer.getHomeRuns() < 50);

        long time = 0;
        int TESTCOUNT = 100;
        for (int i = 0; i < TESTCOUNT; i++) {
            Thread thread1 = new Thread(run1);
            Thread thread2= new Thread(run2);
            Thread thread3 = new Thread(run3);

            thread1.start();
            thread2.start();
            thread3.start();

            try {
                thread1.join();
                thread2.join();
                thread3.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            time += run1.getTime() + run2.getTime() + run3.getTime();
        }

        System.out.println("vrouwen");
        run1.getBaseballers().stream().limit(5).forEach(System.out::println);
        System.out.println("mannen");
        run2.getBaseballers().stream().limit(5).forEach(System.out::println);
        System.out.println("onder 50 homeruns");
        run3.getBaseballers().stream().limit(5).forEach(System.out::println);

        System.out.println(time/TESTCOUNT + "ms");




    }
}
