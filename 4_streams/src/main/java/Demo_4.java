import be.kdg.baseballproject.data.Data;
import be.kdg.baseballproject.model.Baseballer;
import be.kdg.baseballproject.model.Baseballers;
import be.kdg.baseballproject.model.Sex;
import be.kdg.baseballproject.util.Functions;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Demo_4 {
    public static void main(String[] args) {
        List<Baseballer> cleanList = new LinkedList<>(Data.getData());
        Baseballers baseballers = new Baseballers();
        cleanList.forEach(baseballers::voegToe);
        System.out.println("#################### Test sortedBy method ######################");
        baseballers.sortedBy(Baseballer::getName).forEach(System.out::println);

        System.out.println("\n#################### Test functions Alle mannelijke baseballers met een hoger batting average dan 0.3 en meer dan 100 homeruns ######################");
        List<Baseballer> baseballerList = new LinkedList<>(cleanList);
        baseballerList = Functions.filteredList(baseballerList, baseballer -> baseballer.getSex() == Sex.MALE);
        baseballerList = Functions.filteredList(baseballerList, baseballer -> baseballer.getBattingAverage() > 0.3);
        baseballerList = Functions.filteredList(baseballerList, baseballer -> baseballer.getHomeRuns() > 100);
        baseballerList.forEach(System.out::println);

        System.out.println("\n#################### Test functions Gemiddelde ######################");
        System.out.printf("Gemiddeld aantal homeruns: %s\n", Functions.average(cleanList, Baseballer::getHomeRuns));
        System.out.printf("Gemiddeld aantal batting average: %s\n", Functions.average(cleanList, Baseballer::getBattingAverage));
        System.out.printf("Gemiddeld teamnumber: %s\n", Functions.average(cleanList, Baseballer::getTeamNumber));

        System.out.println("\n#################### Test functions countIf ######################");
        System.out.printf("Aantal baseballers met homeruns > 90: %d\n", Functions.countIf(cleanList, baseballer -> baseballer.getHomeRuns() > 90.0));
        System.out.printf("Aantal baseballers waarvan de naam begint met M: %d\n", Functions.countIf(cleanList, baseballer -> baseballer.getName().toUpperCase().startsWith("M")));
        System.out.printf("Aantal baseballers met team nummer hoger dan 20: %d\n", Functions.countIf(cleanList, baseballer -> baseballer.getTeamNumber() > 20.0));

        System.out.println("\n#################### Streaming  ######################");

        System.out.println("------------------- tel -------------------");
        System.out.printf("Aantal spelers van Housten Atros: %d", cleanList.stream().filter(baseballer -> baseballer.getTeam().equals("Houston Atros")).count());

        System.out.println("\n\n------------------- sorteer -------------------");
        System.out.println("Alle baseballers gesorteerd op Team en Nummer");
        cleanList.stream().sorted(Comparator.comparing(Baseballer::getTeam).thenComparing(Baseballer::getTeamNumber)).forEach(System.out::println);
        System.out.println("\nAlle Teams in hoofdletters, omgekeerd gesorteerd zonder dubbels");
        cleanList.stream().sorted(Comparator.comparing(Baseballer::getTeam).reversed()).map(baseballer -> baseballer.getTeam().toUpperCase()).distinct().forEach(System.out::println);

        System.out.println("\n------------------- filer en findany -------------------");
        System.out.println("Een willekeurige baseballer met meer dan 50 homeruns");
        System.out.println(cleanList.stream().filter(baseballer -> baseballer.getHomeRuns() > 50).findAny().orElse(null));

        System.out.println("\nDe jongste baseballer");
        System.out.println(cleanList.stream().max(Comparator.comparing(Baseballer::getBirthDay)).orElse(null));

        System.out.println("\n------------------- selecteer -------------------");
        System.out.println("List van alle spelers waarbij de naam begint met de letter M");
        System.out.println(cleanList.stream().filter(baseballer -> baseballer.getName().toUpperCase().startsWith("M")).map(Baseballer::getName).collect(Collectors.toList()));

        System.out.println("\n------------------- partioneer -------------------");
        Map<Boolean, List<Baseballer>> gepartioneerdOpGeslacht = cleanList.stream().sorted(Comparator.comparing(Baseballer::getSex)).collect(Collectors.partitioningBy(baseballer -> baseballer.getSex() == Sex.MALE));

        System.out.println("Alle mannelijke baseballers");
        gepartioneerdOpGeslacht.get(true).forEach(System.out::println);

        System.out.println("\nAlle vrouwelijke baseballers");
        gepartioneerdOpGeslacht.get(false).forEach(System.out::println);


    }
}
