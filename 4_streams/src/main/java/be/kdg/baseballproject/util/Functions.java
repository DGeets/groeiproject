package be.kdg.baseballproject.util;

import javax.annotation.processing.Completion;
import java.io.ByteArrayInputStream;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;
import java.util.function.ToDoubleFunction;
import java.util.stream.Collectors;

public class Functions {

    public static <T> List<T> filteredList(List<T> baseballerList, Predicate<T> predicate){
        return baseballerList.stream().filter(predicate).collect(Collectors.toList());
    }

    public static <T> Double average (List<T> baseballerList, ToDoubleFunction<T> mapper){
        return baseballerList.stream().mapToDouble(mapper).average().getAsDouble();
    }

    public static <T> long countIf (List<T> baseballerList, Predicate<T> predicate){
        return baseballerList.stream().filter(predicate).count();
    }
}
