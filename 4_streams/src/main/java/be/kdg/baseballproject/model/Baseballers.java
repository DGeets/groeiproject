package be.kdg.baseballproject.model;

import java.util.*;
import java.util.function.Function;

public class Baseballers {
    //region attributes
    private TreeSet<Baseballer> baseballersList;
    //endregion

    //region constructors
    public Baseballers(){
        this.baseballersList = new TreeSet<>();
    }
    //endregion

    //region CRUD methods
    public boolean voegToe(Baseballer baseballer){
        return baseballersList.add(baseballer);
    }

    public boolean verwijder(String team, int teamNumber){
        Baseballer baseballer = baseballersList.stream().filter(b -> b.getTeam().equals(team) && b.getTeamNumber() == teamNumber).findFirst().orElse(null);
        if (baseballer == null) return false;
        return baseballersList.remove(baseballer);
    }

    public Baseballer zoek(String team, int teamNumber){
        return baseballersList.stream().filter(b -> b.getTeam().equals(team) && b.getTeamNumber() == teamNumber).findFirst().orElse(null);
    }
    //endregion

    //region sorting methods
    public List<Baseballer> gesorteerdOpNaam(){
        List<Baseballer> list = new ArrayList<>(baseballersList);
        list.sort(new BaseballerNameComparator());
        return list;
    }

    public List<Baseballer> gesorteerdOpHomeruns(){
        List<Baseballer> list = new ArrayList<>(baseballersList);
        list.sort(new BaseballerHomrunsComparator());
        return list;
    }

    public List<Baseballer> gesorteerdopGeboorte(){
        List<Baseballer> list = new ArrayList<>(baseballersList);
        list.sort(new BaseballerGeboorteComparator());
        return list;
    }

    public List<Baseballer> sortedBy(Function<Baseballer, Comparable> f){
        List<Baseballer> convertList = new LinkedList<>(baseballersList);
        Collections.sort(convertList, Comparator.comparing(f));
        return convertList;
    }


    public int getAantal(){return baseballersList.size();}
    //endregion

    //region private inner classes
    private class BaseballerNameComparator implements Comparator<Baseballer>{
        @Override
        public int compare(Baseballer o1, Baseballer o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }
    private class BaseballerHomrunsComparator implements Comparator<Baseballer>{
        @Override
        public int compare(Baseballer o1, Baseballer o2) {
            return Integer.compare(o1.getHomeRuns(), o2.getHomeRuns());
        }
    }
    private class BaseballerGeboorteComparator implements Comparator<Baseballer>{
        @Override
        public int compare(Baseballer o1, Baseballer o2) {
            if (o1.getBirthDay().isBefore(o2.getBirthDay())){
                return 1;
            }else if (o1.getBirthDay().isAfter(o2.getBirthDay())){
                return -1;
            }
            else return 0;
        }
    }
    //endregion


}
